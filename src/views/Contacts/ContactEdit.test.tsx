import React from 'react';
import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { MockedProvider } from '@apollo/react-testing';

import { updateContactMock, getContactMock, contact } from '../../mocks';
import ContactEdit from './ContactEdit';

const history = createMemoryHistory();

const component = (
  <MockedProvider
    mocks={[...updateContactMock, ...getContactMock]}
    addTypename={false}
  >
    {/* 
       // @ts-ignore */}
    <ContactEdit
      match={{ params: { id: contact.id }, isExact: true, path: '', url: '' }}
      history={history}
    />
  </MockedProvider>
);

test('it renders with loading state initially', () => {
  const { getByText, container } = render(component);
  expect(getByText('Loading...')).toBeDefined();
  expect(container.firstChild).toMatchSnapshot();
});

test('it renders a dialog with correct title', async () => {
  const { findByText } = render(component);
  const title = await findByText('Edit Contact');
  expect(title).toMatchSnapshot();
});

test('it renders a dialog with correct description', async () => {
  const { findByText } = render(component);
  const title = await findByText('Enter a name and email to edit contact');
  expect(title).toMatchSnapshot();
});

test('it should render an input a value of "Janet Doe"', async () => {
  const { findByDisplayValue } = render(component);
  const name = await findByDisplayValue(contact.name);
  expect(name).toBeDefined();
  expect(name).toMatchSnapshot();
});

test('it should render an input a value of "janet@doe.com"', async () => {
  const { findByDisplayValue } = render(component);
  const email = await findByDisplayValue(contact.email);
  expect(email).toBeDefined();
  expect(email).toMatchSnapshot();
});
