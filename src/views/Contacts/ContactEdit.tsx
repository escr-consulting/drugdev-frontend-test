import React from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { RouteComponentProps } from 'react-router-dom';

import { Contact, InputContact } from '../../interfaces';
import { GET_CONTACT, UPDATE_CONTACT } from '../../queries';

import ContactsForm from '../../components/Contacts/ContactsForm';

interface TParams {
  history: string;
  id: string;
}

const ContactEdit: React.FC<RouteComponentProps<TParams>> = ({
  history,
  match: {
    params: { id },
  },
}) => {
  const { loading, error, data } = useQuery(GET_CONTACT, {
    variables: {
      id,
    },
  });
  const [updateContact] = useMutation(UPDATE_CONTACT);

  const originalContact: Contact = (data && data.contact) || {};

  if (loading) return <div>Loading...</div>;
  if (error) return <div>`Error! ${error}`</div>;

  return (
    <ContactsForm
      title="Edit Contact"
      contact={originalContact}
      onCancel={(): void => history.push('/')}
      onSubmit={(contact: InputContact): void => {
        contact.id = originalContact.id;
        updateContact({ variables: { contact } });
        history.push('/view_contact/' + id);
      }}
    />
  );
};

export default ContactEdit;
