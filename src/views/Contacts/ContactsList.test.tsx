import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { MockedProvider } from '@apollo/react-testing';

import {
  getContactsMock,
  deleteContactMock,
  getEmptyContactsMock,
} from '../../mocks';
import ContactsList from './ContactsList';

const history = createMemoryHistory();

const component = (
  <MockedProvider
    mocks={[...getContactsMock, ...deleteContactMock, ...getContactsMock]}
    addTypename={false}
  >
    {/* 
       // @ts-ignore */}
    <ContactsList history={history} />
  </MockedProvider>
);

test('it renders with loading state initially', () => {
  const { getByText, container } = render(component);
  expect(getByText('Loading...')).toBeDefined();
  expect(container.firstChild).toMatchSnapshot();
});

// test('it should delete contact when Delete button is clicked', async () => {
//   const { findByTestId, findByText } = render(component);
//   const deleteBtn = await findByTestId('delete');

//   fireEvent.click(deleteBtn);

//   const addContactBtn = await findByText('Add Contact');
//   expect(addContactBtn).toBeDefined();
// });
