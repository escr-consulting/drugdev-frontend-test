import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { MockedProvider } from '@apollo/react-testing';

import { addContactMock } from '../../mocks';
import ContactAdd from './ContactAdd';

const history = createMemoryHistory();

const component = (
  <MockedProvider mocks={addContactMock} addTypename={false}>
    {/* 
       // @ts-ignore */}
    <ContactAdd history={history} />
  </MockedProvider>
);

test('it renders a dialog with correct title', () => {
  const { getByText } = render(component);
  const title = getByText('Add Contact');
  expect(title).toMatchSnapshot();
});

test('it renders a dialog with correct description', () => {
  const { getByText } = render(component);
  const description = getByText('Enter a name and email to add contact');
  expect(description).toMatchSnapshot();
});
