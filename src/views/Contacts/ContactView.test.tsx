import React from 'react';
import { render } from '@testing-library/react';
import { MockedProvider } from '@apollo/react-testing';

import { getContactMock, contact } from '../../mocks';
import ContactView from './ContactView';

const component = (
  <MockedProvider mocks={getContactMock} addTypename={false}>
    {/* 
       // @ts-ignore */}
    <ContactView
      match={{ params: { id: contact.id }, isExact: true, path: '', url: '' }}
    />
  </MockedProvider>
);

test('it renders with loading state initially', () => {
  const { getByText, container } = render(component);
  expect(getByText('Loading...')).toBeDefined();
  expect(container.firstChild).toMatchSnapshot();
});

test('it should render a contact with name, email, modified and created date', async () => {
  const { findByText, asFragment } = render(component);
  const name = await findByText(contact.name);
  const email = await findByText(contact.email);

  expect(name).toBeDefined();
  expect(email).toBeDefined();
  expect(asFragment()).toMatchSnapshot();
});
