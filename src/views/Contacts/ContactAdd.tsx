import React from 'react';
import { useMutation } from '@apollo/react-hooks';
import { RouteComponentProps } from 'react-router-dom';

import { InputContact } from '../../interfaces';
import { ADD_CONTACT } from '../../queries';

import ContactsForm from '../../components/Contacts/ContactsForm';

interface TParams {
  history: string;
}

const ContactAdd: React.FC<RouteComponentProps<TParams>> = ({ history }) => {
  const [addContact] = useMutation(ADD_CONTACT);

  const initialContact: InputContact = { name: '', email: '' };

  return (
    <ContactsForm
      title="Add Contact"
      contact={initialContact}
      onCancel={(): void => history.push('/')}
      onSubmit={(contact: InputContact): void => {
        addContact({ variables: { contact } });
        history.push('/');
      }}
    />
  );
};

export default ContactAdd;
