import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { RouteComponentProps } from 'react-router-dom';

import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

import ContactsCard from '../../components/Contacts/ContactsCard';

import { Contact } from '../../interfaces';
import { GET_CONTACT } from '../../queries';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
  }),
);

interface TParams {
  id: string;
}

const ContactView: React.FC<RouteComponentProps<TParams>> = ({
  match: {
    params: { id },
  },
}) => {
  const { loading, error, data } = useQuery(GET_CONTACT, {
    variables: {
      id,
    },
  });

  const contact: Contact = (data && data.contact) || {};
  const classes = useStyles();

  if (loading) return <div>Loading...</div>;
  if (error) return <div>`Error! ${error}`</div>;

  return (
    <div className={classes.root}>
      <ContactsCard contact={contact} />
    </div>
  );
};

export default ContactView;
