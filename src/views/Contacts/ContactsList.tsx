import React, { useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { RouteComponentProps } from 'react-router-dom';

import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

import Avatar from '../../components/Avatar/Avatar';

import { Contact, ContactsData } from '../../interfaces';
import { GET_CONTACTS, DELETE_CONTACT } from '../../queries';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    list: {
      display: 'block',
    },
    noContacts: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: theme.spacing(8),
    },
  }),
);

interface TParams {
  history: string;
}

const ContactsList: React.FC<RouteComponentProps<TParams>> = ({ history }) => {
  const { loading, error, data, refetch } = useQuery(GET_CONTACTS);
  const [deleteContact] = useMutation(DELETE_CONTACT);

  useEffect(() => {
    refetch();
  });

  const contacts: ContactsData = (data && data.contacts) || [];
  const classes = useStyles();

  if (loading) return <div>Loading...</div>;
  if (error) return <div>`Error! ${error}`</div>;

  return (
    <div className={classes.root}>
      {contacts.length ? (
        <List className={classes.list}>
          {contacts.map((contact: Contact) => (
            <ListItem
              button
              key={contact.id}
              onClick={() => history.push(`/view_contact/${contact.id}`)}
            >
              <ListItemIcon>
                <Avatar name={contact.name} />
              </ListItemIcon>
              <ListItemText
                primary={contact.name}
                secondary={contact.email}
                primaryTypographyProps={{
                  noWrap: true,
                  display: 'block',
                }}
              />
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  aria-label="delete"
                  data-testid="delete"
                  onClick={() => {
                    deleteContact({ variables: { id: contact.id } });
                    refetch();
                  }}
                >
                  <DeleteIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      ) : (
        <div className={classes.noContacts}>
          <Button onClick={() => history.push('/add_contact')}>
            {'Add Contact'} <AddIcon />
          </Button>
        </div>
      )}
    </div>
  );
};

export default ContactsList;
