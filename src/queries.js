import { gql } from 'apollo-boost';

export const GET_CONTACTS = gql`
  query getContacts {
    contacts {
      id
      name
      email
    }
  }
`;

export const GET_CONTACT = gql`
  query getContact($id: ID!) {
    contact(id: $id) {
      id
      name
      email
      createdAt
      modifiedAt
    }
  }
`;

export const UPDATE_CONTACT = gql`
  mutation updateContact($contact: InputContact!) {
    updateContact(contact: $contact) {
      id
      name
      email
    }
  }
`;

export const ADD_CONTACT = gql`
  mutation addContact($contact: InputContact!) {
    addContact(contact: $contact) {
      id
      name
      email
    }
  }
`;

export const DELETE_CONTACT = gql`
  mutation deleteContact($id: ID!) {
    deleteContact(id: $id)
  }
`;
