import React from 'react';
import { render } from '@testing-library/react';

import Avatar from './Avatar';

const name = 'John Smith';
const initialsFromName = 'JS';

test('it renders correctly', () => {
  const { container } = render(<Avatar name={name} />);
  expect(container.firstChild).toMatchSnapshot();
});

test('it renders initals from full name', () => {
  const { getByText } = render(<Avatar name={name} />);
  expect(getByText(initialsFromName).textContent).toBe(initialsFromName);
});
