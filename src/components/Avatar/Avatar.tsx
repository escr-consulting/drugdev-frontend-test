import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import _Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      backgroundColor: theme.palette.secondary.main,
    },
  }),
);

interface IProps {
  name: string;
}

export const Avatar: React.FC<IProps> = ({ name }) => {
  const classes = useStyles();

  return (
    <_Avatar className={classes.avatar}>
      {name.split(' ').map(n => n[0].toUpperCase())}
    </_Avatar>
  );
};

export default Avatar;
