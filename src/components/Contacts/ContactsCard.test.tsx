import React from 'react';
import { render } from '@testing-library/react';

import { contact } from '../../mocks';

import ContactsCard from './ContactsCard';

const component = <ContactsCard contact={contact} />;

test('it renders correctly', () => {
  const { container } = render(component);
  expect(container.firstChild).toMatchSnapshot();
});

test('it renders an Avatar with initials from name', () => {
  const initials = 'JD';
  const { getByText } = render(component);
  expect(getByText(initials).textContent).toBe(initials);
  expect(getByText(initials).firstChild).toMatchSnapshot();
});
