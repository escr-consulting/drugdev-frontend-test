import React, { useState } from 'react';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

import { InputContact } from '../../interfaces';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      maxWidth: '100%',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: theme.palette.primary.main,
    },
    form: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
    },
    actions: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    hide: {
      display: 'none',
    },
  }),
);

interface IProps {
  title: string;
  contact: InputContact;
  onSubmit: (contact: InputContact) => void;
  onCancel: () => void;
}

export const ContactsForm: React.FC<IProps> = ({
  title,
  contact: { name, email },
  onSubmit,
  onCancel,
}) => {
  const classes = useStyles();

  const [nameInput, setNameInput] = useState(name);
  const [emailInput, setEmailInput] = useState(email);
  const [nameError, setNameError] = useState('');
  const [emailError, setEmailError] = useState('');

  const handleSubmit = (
    event:
      | React.FormEvent<HTMLFormElement>
      | React.MouseEvent<HTMLButtonElement>,
  ): void => {
    event.preventDefault();
    if (validateName(contact.name) && validateEmail(contact.email))
      onSubmit(contact);
  };

  const validateName = (name: string): boolean => {
    if (!name) {
      setNameError('Please enter a name');
      return false;
    }

    if (name.split(' ').length > 3) {
      setNameError('No more than three words');
      return false;
    }

    setNameError('');
    return true;
  };

  const validateEmail = (email: string): boolean => {
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!email) {
      setEmailError('Please enter an email');
      return false;
    }

    if (!emailRegex.test(email)) {
      setEmailError('Please enter a valid email');
      return false;
    }
    setEmailError('');
    return true;
  };

  const contact: InputContact = { id: '', name: nameInput, email: emailInput };

  return (
    <Dialog open aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {`Enter a name and email to ${title.toLowerCase()}`}
        </DialogContentText>
        <form
          onSubmit={handleSubmit}
          className={classes.form}
          noValidate
          autoComplete="off"
        >
          <TextField
            autoFocus
            placeholder={name}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              validateName(event.target.value);
              setNameInput(event.target.value);
            }}
            value={nameInput}
            error={!!nameError}
            helperText={nameError && nameError}
            label="Name"
            type="text"
            fullWidth
          />
          <TextField
            placeholder={email}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              validateEmail(event.target.value);
              setEmailInput(event.target.value);
            }}
            value={emailInput}
            error={Boolean(emailError)}
            helperText={emailError && emailError}
            label="Email Address"
            type="email"
            fullWidth
          />
          <input type="submit" className={classes.hide} />
        </form>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={onCancel}>
          Cancel
        </Button>
        <Button
          color="primary"
          disabled={Boolean(nameError || emailError)}
          onClick={handleSubmit}
        >
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ContactsForm;
