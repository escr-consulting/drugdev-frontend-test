import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';

import { Contact } from '../../interfaces';

import Avatar from '../Avatar/Avatar';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      maxWidth: '100%',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: theme.palette.primary.main,
    },
    content: {
      display: 'flex',
      justifyContent: 'space-between',
    },
  }),
);

interface IProps {
  contact: Contact;
}

export const ContactsCard: React.FC<IProps> = ({
  contact: { id, name, email, createdAt, modifiedAt },
}) => {
  const classes = useStyles();

  return (
    <Card key={id} className={classes.card}>
      <CardHeader
        avatar={<Avatar name={name} />}
        action={
          <IconButton aria-label="edit">
            <a href={`/edit_contact/${id}`}>
              <EditIcon />
            </a>
          </IconButton>
        }
        title={name}
        subheader={email}
        titleTypographyProps={{
          noWrap: true,
          display: 'block',
          style: { width: '375px' },
        }}
      />
      <CardContent className={classes.content}>
        <Typography variant="caption" component="span" noWrap>
          {'Created:'} {moment(createdAt).format('MMMM Do YYYY, h:mm:ss a')}
        </Typography>
        <Typography variant="caption" component="span" noWrap>
          {'Modified:'} {moment(modifiedAt).fromNow()}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default ContactsCard;
