import React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      textTransform: 'capitalize',
    },
  }),
);

interface TParams {
  history: string;
}

export const Header: React.FC<RouteComponentProps<TParams>> = ({ history }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            onClick={handleClick}
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem
              onClick={() => {
                handleClose();
                history.push('/contacts_list');
              }}
            >
              Contacts List
            </MenuItem>
            <MenuItem
              onClick={() => {
                handleClose();
                history.push('/add_contact');
              }}
            >
              Add Contact
            </MenuItem>
          </Menu>
          <Typography variant="h5" className={classes.title}>
            {history.location.pathname
              .split('/')
              .map((c, i) => i < 2 && c.split('_').join(' '))}
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
