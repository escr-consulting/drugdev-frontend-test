import React from 'react';
import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';

import Header from './Header';

const history = createMemoryHistory({ initialEntries: ['/contacts_list'] });

test('it renders correctly', () => {
  const { container } = render(
    <>
      {/* 
       // @ts-ignore */}
      <Header history={history} />
    </>,
  );
  expect(container.firstChild).toMatchSnapshot();
});
