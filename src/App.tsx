import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';

import Container from '@material-ui/core/Container';

import Header from './components/Header/Header';
import ContactsList from './views/Contacts/ContactsList';
import ContactAdd from './views/Contacts/ContactAdd';
import ContactEdit from './views/Contacts/ContactEdit';
import ContactView from './views/Contacts/ContactView';

import './App.css';

const client = new ApolloClient({
  uri: 'http://localhost:3001',
});

const App: React.FC = () => {
  return (
    <ApolloProvider client={client}>
      <Container component="main" maxWidth="sm">
        <BrowserRouter>
          <Route exact path="/">
            <Redirect to="/contacts_list" />
          </Route>
          <Route component={Header} path="/" />
          <div
            style={{
              marginTop: 4,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Route component={ContactsList} exact path="/contacts_list" />
            <Route component={ContactView} path="/view_contact/:id" />
            <Route component={ContactEdit} path="/edit_contact/:id" />
            <Route component={ContactAdd} exact path="/add_contact" />
          </div>
        </BrowserRouter>
      </Container>
    </ApolloProvider>
  );
};

export default App;
