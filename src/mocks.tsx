import {
  GET_CONTACT,
  GET_CONTACTS,
  UPDATE_CONTACT,
  DELETE_CONTACT,
  ADD_CONTACT,
} from './queries';

export const contact = {
  name: 'Janet Doe',
  email: 'jane@doe.com',
  id: 'SZuuoBIHVRYuWGWavPYH2',
  createdAt: '2019-10-18T11:46:18.250Z',
  modifiedAt: new Date(Date.now()).toISOString(),
};

export const getContactMock = [
  {
    request: {
      query: GET_CONTACT,
      variables: {
        id: contact.id,
      },
    },
    result: {
      data: {
        contact,
      },
    },
  },
];
export const getContactsMock = [
  {
    request: {
      query: GET_CONTACTS,
    },
    result: {
      data: { contacts: [{ ...contact }] },
    },
  },
];

export const getEmptyContactsMock = [
  {
    request: {
      query: GET_CONTACTS,
    },
    result: {
      data: { contacts: [] },
    },
  },
];

export const updateContactMock = [
  {
    request: {
      query: UPDATE_CONTACT,
      variables: {
        contact,
      },
    },
    result: {
      data: {
        contact,
      },
    },
  },
];

export const addContactMock = [
  {
    request: {
      query: ADD_CONTACT,
      variables: {
        contact,
      },
    },
    result: {
      data: {
        contact,
      },
    },
  },
];
export const deleteContactMock = [
  {
    request: {
      query: DELETE_CONTACT,
      variables: {
        id: contact.id,
      },
    },
    result: {
      data: { contact },
    },
  },
];
