export interface Contact {
  id: string;
  name: string;
  email: string;
  createdAt?: string;
  modifiedAt?: string;
}

export interface InputContact {
  id?: string;
  name: string;
  email: string;
}

export interface ContactsData {
  [x: string]: any;
  contacts: Array<Contact>;
}
